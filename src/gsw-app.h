/*
 * gsw-app.h - Header file for gsw-app.c.
 * 
 * Copyright (C) 2004 Archit Baweja
 *
 * Author(s):
 *	Archit Baweja <bighead@users.sourceforge.net>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 */

#ifndef _GSW_APP_H_
#define _GSW_APP_H_

G_BEGIN_DECLS


#define GSW_TYPE_APP		(gsw_app_get_type ())
#define GSW_APP(obj)		(G_TYPE_CHECK_INSTANCE_CAST ((obj), GSW_TYPE_APP, GswApp))
#define GSW_APP_CLASS(klass)	(G_TYPE_CHECK_CLASS_CAST ((klass), GSW_TYPE_APP, GswAppClass))
#define GSW_IS_APP(obj)		(G_TYPE_CHECK_INSTANCE_TYPE ((obj), GSW_TYPE_APP))
#define GSW_IS_APP_CLASS(klass)	(G_TYPE_CHECK_CLASS_TYPE ((klass), GSW_TYPE_APP))
#define GSW_APP_GET_CLASS(obj)	(G_TYPE_INSTANCE_GET_CLASS ((obj), GSW_TYPE_APP, GswAppClass))

typedef struct _GswApp		GswApp;
typedef struct _GswAppClass	GswAppClass;
typedef struct _GswAppPrivate	GswAppPrivate;

struct _GswApp {
	GtkWindow __parent__;

	GswAppPrivate *priv;
};

struct _GswAppClass {
	GtkWindowClass __parent__;
};

GType		gsw_app_get_type	(void);
GtkWidget *	gsw_app_new		(void);


G_END_DECLS

#endif /* _GSW_APP_H_ */
