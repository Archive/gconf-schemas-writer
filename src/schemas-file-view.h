/*
 * schemas-file-view.h - Header file for schemas-file-view.c.
 * 
 * Copyright (C) 2004 Archit Baweja
 *
 * Author(s):
 *	Archit Baweja <bighead@users.sourceforge.net>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 */

#ifndef _SCHEMAS_FILE_VIEW_H_
#define _SCHEMAS_FILE_VIEW_H_

G_BEGIN_DECLS


#include "schemas-file.h"

#define TYPE_SCHEMAS_FILE_VIEW		  (schemas_file_view_get_type ())
#define SCHEMAS_FILE_VIEW(obj)		  (G_TYPE_CHECK_INSTANCE_CAST ((obj), TYPE_SCHEMAS_FILE_VIEW, SchemasFileView))
#define SCHEMAS_FILE_VIEW_CLASS(klass)	  (G_TYPE_CHECK_CLASS_CAST ((klass), TYPE_SCHEMAS_FILE_VIEW, SchemasFileViewClass))
#define IS_SCHEMAS_FILE_VIEW(obj)	  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), TYPE_SCHEMAS_FILE_VIEW))
#define IS_SCHEMAS_FILE_VIEW_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), TYPE_SCHEMAS_FILE_VIEW))
#define SCHEMAS_FILE_VIEW_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), TYPE_SCHEMAS_FILE_VIEW, SchemasFileViewClass))

typedef struct _SchemasFileView		SchemasFileView;
typedef struct _SchemasFileViewClass	SchemasFileViewClass;
typedef struct _SchemasFileViewPrivate	SchemasFileViewPrivate;

struct _SchemasFileView {
	GtkVBox __parent__;

	SchemasFileViewPrivate *priv;
};

struct _SchemasFileViewClass {
	GtkVBoxClass __parent__;
};

GType		schemas_file_view_get_type	(void);
GtkWidget *	schemas_file_view_new		(GtkUIManager *ui_manager);

void		schemas_file_view_load		(SchemasFileView *sfile_view,
						 SchemasFile *sfile);


G_END_DECLS

#endif /* _SCHEMAS_FILE_VIEW_H_ */
