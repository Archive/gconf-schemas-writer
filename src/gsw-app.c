#include <stdio.h>
#include <stdlib.h>

#include <gtk/gtk.h>

#include "gsw-app.h"
#include "schemas-file-view.h"

#define GSW_APP_DEFAULT_WSIZE 400
#define GSW_APP_DEFAULT_HSIZE 400

/* Private section of the GswApp class. */
struct _GswAppPrivate {
	GtkWidget *main_vbox;
	GtkUIManager *ui_manager;

	GtkWidget *sfile_view;
};

static GtkWindowClass *parent_class = NULL;

static void
gsw_app_destroy (GtkObject *obj)
{
	GswApp *app;

	g_return_if_fail (obj != NULL);
	g_return_if_fail (GSW_IS_APP (obj));

	app = GSW_APP (obj);

	if (app->priv->sfile_view != NULL) {
		gtk_widget_destroy (GTK_WIDGET (app->priv->sfile_view));
		app->priv->sfile_view = NULL;
	}

	if (app->priv->main_vbox != NULL) {
		gtk_widget_destroy (GTK_WIDGET (app->priv->main_vbox));
		app->priv->main_vbox = NULL;
	}

	if (GTK_OBJECT_CLASS (parent_class)->destroy)
		GTK_OBJECT_CLASS (parent_class)->destroy (obj);
}

static void
gsw_app_finalize (GObject *obj)
{
	GswApp *app;

	g_return_if_fail (GSW_IS_APP (obj));

	app = GSW_APP (obj);
	g_free (app->priv);
	app->priv = NULL;

	if (G_OBJECT_CLASS (parent_class)->finalize)
		(* G_OBJECT_CLASS (parent_class)->finalize) (obj);
}

static void
gsw_app_init (GswApp *app)
{
	GswAppPrivate *priv;

	/* Create a new structure of private variables and initialize */
	priv = g_new0 (GswAppPrivate, 1);
	priv->main_vbox = NULL;
	priv->sfile_view = NULL;

	app->priv = priv;
}

static void
gsw_app_class_init (GswAppClass *klass)
{
	GObjectClass	*gobject_class;
	GtkObjectClass	*object_class;
	GtkWidgetClass	*widget_class;

	gobject_class = (GObjectClass *) klass;
	object_class = (GtkObjectClass *) klass;
	widget_class = (GtkWidgetClass *) klass;

	parent_class = g_type_class_peek_parent (klass);

	object_class->destroy = gsw_app_destroy;
	gobject_class->finalize = gsw_app_finalize;
}

static gint
gsw_app_delete_event_cb (GtkWidget *widget, gpointer user_data)
{
	gtk_main_quit ();
	return FALSE;
}

static void
action_FileNew_cb ()
{
	g_message ("%s: implement me!", __FUNCTION__);
}

static void
action_FileExit_cb (GtkWidget *app)
{
	gtk_main_quit ();
}

static void
action_HelpAbout_cb ()
{
	g_message ("%s: implement me!", __FUNCTION__);
}

static GtkActionEntry entries[] = {
	{ "FileMenu", NULL, "_File" },
	{ "New", GTK_STOCK_NEW, "_New", "<control>N", "Start a new schema file", action_FileNew_cb },
	{ "Exit", GTK_STOCK_QUIT, "E_xit", "<control>Q", "Exit gconf-schema-writer", action_FileExit_cb },

	{ "HelpMenu", NULL, "_Help" },
	{ "About", NULL, "About", NULL, "About gconf-schema-writer", action_HelpAbout_cb },
};

static const char *ui_description =
"<ui>"
"  <menubar name='MainMenu'>"
"    <menu name='FileMenu' action='FileMenu'>"
"      <menuitem action='New'/>"
"      <placeholder name='FileMenuAdditions'/>"
"      <separator/>"
"      <menuitem action='Exit'/>"
"    </menu>"
"    <placeholder name='MainMenuAdditions'/>"
"    <menu name='HelpMenu' action='HelpMenu'>"
"      <menuitem action='About'/>"
"    </menu>"
"  </menubar>"
"  <toolbar action='toolbar1'>"
"    <toolitem action='New'/>"
"    <placeholder name='ToolbarAdditions'/>"
"  </toolbar>"
"</ui>";

static void
gsw_app_construct (GswApp *app)
{
	GtkWidget *menubar;
	GtkWidget *toolbar;
	GtkActionGroup *action_group;
	GtkUIManager *ui_manager;
	GtkAccelGroup *accel_group;
	GError *error;

	/* Set default geometry and title. */
	gtk_window_set_title (GTK_WINDOW (app), "GConf Schema Writer");
	gtk_window_set_default_size (GTK_WINDOW (app),
				     GSW_APP_DEFAULT_WSIZE,
				     GSW_APP_DEFAULT_HSIZE);
	gtk_window_set_resizable (GTK_WINDOW (app), TRUE);
	g_signal_connect (G_OBJECT (app), "delete_event",
			  G_CALLBACK (gsw_app_delete_event_cb),
			  (gpointer) app);

	app->priv->main_vbox = gtk_vbox_new (FALSE, 0);
	gtk_container_add (GTK_CONTAINER (app), app->priv->main_vbox);
	gtk_widget_show (app->priv->main_vbox);

	/* Create and insert the menu's and toolbars. */
	action_group = gtk_action_group_new ("MainActions");
	gtk_action_group_add_actions (action_group, entries,
				      G_N_ELEMENTS (entries),
				      GTK_WIDGET (app));

	ui_manager = gtk_ui_manager_new ();
	gtk_ui_manager_insert_action_group (ui_manager, action_group, 0);

	accel_group = gtk_ui_manager_get_accel_group (ui_manager);
	gtk_window_add_accel_group (GTK_WINDOW (app), accel_group);

	error = NULL;
	if (!gtk_ui_manager_add_ui_from_string (ui_manager,
						ui_description, -1, &error)) {
		g_message ("building menus failed: %s", error->message);
		g_error_free (error);
		gtk_main_quit ();
		exit (EXIT_FAILURE);
	}
	app->priv->ui_manager = ui_manager;

	/* The main menubar. */
	menubar = gtk_ui_manager_get_widget (ui_manager, "/MainMenu");
	gtk_box_pack_start (GTK_BOX (app->priv->main_vbox), menubar,
			    FALSE, FALSE, 0);
	gtk_widget_show (menubar);

	toolbar = gtk_ui_manager_get_widget (ui_manager, "/toolbar1");
	gtk_toolbar_set_style (GTK_TOOLBAR (toolbar), GTK_TOOLBAR_ICONS);
	gtk_box_pack_start (GTK_BOX (app->priv->main_vbox), toolbar,
			    FALSE, FALSE, 0);
	gtk_widget_show (toolbar);

	/* The SchemasFileView control. */
	app->priv->sfile_view = schemas_file_view_new (ui_manager);
	gtk_box_pack_start (GTK_BOX (app->priv->main_vbox), 
			    app->priv->sfile_view,
			    TRUE, TRUE, 0);
	gtk_widget_show (app->priv->sfile_view);
}

GType
gsw_app_get_type ()
{
	static GType gsw_app_type = 0;

	if (!gsw_app_type) {
		static const GTypeInfo gsw_app_type_info = {
			sizeof (GswAppClass),
			NULL,
			NULL,
			(GClassInitFunc) gsw_app_class_init,
			NULL,
			NULL,
			sizeof (GswApp),
			0,
			(GInstanceInitFunc) gsw_app_init,
		};

		gsw_app_type = g_type_register_static (GTK_TYPE_WINDOW, "GswApp", &gsw_app_type_info, 0);
	}

	return gsw_app_type;
}

GtkWidget *
gsw_app_new ()
{
	GtkWidget *app;

	app = GTK_WIDGET (g_object_new (GSW_TYPE_APP, NULL));
	gsw_app_construct (GSW_APP (app));

	return app;
}
