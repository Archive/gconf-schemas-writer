/*
 * schmeas-file.h - Header file for schemas-file.h.
 * 
 * Copyright (C) 2004 Archit Baweja
 *
 * Author(s):
 *	Archit Baweja <bighead@users.sourceforge.net>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 */

#ifndef _SCHEMAS_FILE_H_
#define _SCHEMAS_FILE_H_

G_BEGIN_DECLS

typedef struct _SchemasFile SchemasFile;

struct _SchemasFile {
	gchar *filename;

	GList *schemas_list;
};

SchemasFile *		schemas_file_new	(const gchar *filename);
void			schemas_file_free	(SchemasFile *sfile);

gboolean		schemas_file_save	(SchemasFile *sfile,
						 const gchar *filename);

G_END_DECLS

#endif /* _SCHEMAS_FILE_H_ */
