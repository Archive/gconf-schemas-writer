/*
 * schemas-file.c - An object representing a .schemas file.
 * 
 * Copyright (C) 2004 Archit Baweja
 *
 * Author(s):
 *	Archit Baweja <bighead@users.sourceforge.net>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <glib.h>
#include <libxml/xmlmemory.h>
#include <libxml/parser.h>

#include "schemas-file.h"
#include "schemas-file-xml-reader.h"
#include "schema.h"

static SchemasFile *
schemas_file_init ()
{
	SchemasFile *sfile;

	sfile = g_new0 (SchemasFile, 1);
	sfile->filename = NULL;
	sfile->schemas_list = NULL;

	return sfile;

}

SchemasFile *
schemas_file_new (const gchar *filename)
{
	SchemasFile *sfile;
	SchemasFileXMLReader *reader;

	sfile = schemas_file_init ();

	if (filename == NULL) {
		sfile->filename = NULL;
		return NULL;
	} else {
		sfile->filename = g_strdup (filename);
	}

	reader = schemas_file_xml_reader_new (filename);
	sfile->schemas_list = g_list_copy (schemas_file_xml_reader_get_schemas_list (reader));
	schemas_file_xml_reader_free (reader);

	return sfile;
}

void
schemas_file_free (SchemasFile *sfile)
{
	if (sfile->filename != NULL) {
		g_free (sfile->filename);
		sfile->filename = NULL;
	}

	if (sfile->schemas_list != NULL) {
		g_list_free (sfile->schemas_list);
		sfile->schemas_list = NULL;
	}
}

gboolean
schemas_file_save (SchemasFile *sfile, const gchar *filename)
{
	/* FIXME: Use XMLSchemasFileWriter */
	return TRUE;
}
