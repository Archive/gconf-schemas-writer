/*
 * schema.c - Holds schema information for each key.
 * 
 * Copyright (C) 2004 Archit Baweja
 *
 * Author(s):
 *	Archit Baweja <bighead@users.sourceforge.net>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 */

#include <glib.h>
#include "schema.h"

Schema *
schema_new ()
{
	Schema *s = NULL;

	s = g_new0 (Schema, 1);
	s->key = NULL;
	s->applyto = NULL;
	s->owner = NULL;
	s->type = NULL;
	s->default_val = NULL;
	s->locale_descrip_list = NULL;

	return s;
}

void
schema_free (Schema *s)
{
	if (s->key != NULL) {
		g_free (s->key);
		s->key = NULL;
	}

	if (s->applyto != NULL) {
		g_free (s->applyto);
		s->applyto = NULL;
	}

	if (s->owner != NULL) {
		g_free (s->owner);
		s->owner = NULL;
	}

	if (s->type != NULL) {
		g_free (s->type);
		s->type = NULL;
	}

	if (s->default_val != NULL) {
		g_free (s->default_val);
		s->default_val = NULL;
	}

	if (s->locale_descrip_list != NULL) {
		g_list_free (s->locale_descrip_list);
		s->locale_descrip_list = NULL;
	}

	g_free (s);
}
