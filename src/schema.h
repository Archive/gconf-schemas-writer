/*
 * schema.h - Header file for schema.c
 * 
 * Copyright (C) 2004 Archit Baweja
 *
 * Author(s):
 *	Archit Baweja <bighead@users.sourceforge.net>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 */

#ifndef _SCHEMA_H_
#define _SCHEMA_H_ 

G_BEGIN_DECLS

typedef struct _Schema Schema;

struct _Schema {
	gchar *key;
	gchar *applyto;

	gchar *owner;

	gchar *type;

	gchar *default_val;

	GList *locale_descrip_list;
};


Schema *	schema_new	(void);
void		schema_free	(Schema *s);

G_END_DECLS

#endif /* _SCHEMA_H_ */
