/*
 * schemas-file-xml-reader.h - Header file for schemas-file-xml-reader.c.
 * 
 * Copyright (C) 2004 Archit Baweja
 *
 * Author(s):
 *	Archit Baweja <bighead@users.sourceforge.net>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 */

#ifndef _SCHEMAS_FILE_XML_READER_H_
#define _SCHEMAS_FILE_XML_READER_H_

G_BEGIN_DECLS

#include <libxml/parser.h>

typedef struct _SchemasFileXMLReader SchemasFileXMLReader;

struct _SchemasFileXMLReader {
	xmlDocPtr doc;
	gchar *filename;

	GList *schemas_list;
};

SchemasFileXMLReader * schemas_file_xml_reader_new
						(const gchar *filename);
void		       schemas_file_xml_reader_free 
						(SchemasFileXMLReader *this);

const GList *	       schemas_file_xml_reader_get_schemas_list
						(SchemasFileXMLReader *this);


G_END_DECLS

#endif /* _SCHEMAS_FILE_XML_READER_H_ */
