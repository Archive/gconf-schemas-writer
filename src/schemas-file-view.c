/*
 * schemas-file-view.c - A view for the SchemaFile object/model.
 * 
 * Copyright (C) 2004 Archit Baweja
 *
 * Author(s):
 *	Archit Baweja <bighead@users.sourceforge.net>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 */

#include <stdio.h>
#include <stdlib.h>

#include <gtk/gtk.h>

#include "schemas-file-view.h"
#include "schemas-file.h"
#include "schema.h"
#include "schema-editor-dialog.h"

struct _SchemasFileViewPrivate {
	GtkUIManager *ui_manager;

	/* The model for the .schemas file. */
	SchemasFile *sfile;

	/* The List View, the main view! */
	GtkWidget *list_view;
};

static GtkVBoxClass *parent_class = NULL;

/* Used by the GtkTreeView code. */
enum {
	COL_OWNER = 0,
	COL_KEY,
	COL_TYPE,
	COL_PTR,
	NUM_COLS
};

static void
schemas_file_view_destroy (GtkObject *obj)
{
	SchemasFileView *sfile_view;

	sfile_view = SCHEMAS_FILE_VIEW (obj);

	if (sfile_view->priv->ui_manager != NULL) {
		g_object_unref (G_OBJECT (sfile_view->priv->ui_manager));
		sfile_view->priv->ui_manager = NULL;
	}

	if (sfile_view->priv->sfile != NULL) {
		schemas_file_free (sfile_view->priv->sfile);
		sfile_view->priv->sfile = NULL;
	}

	if (GTK_OBJECT_CLASS (parent_class)->destroy)
		(* GTK_OBJECT_CLASS (parent_class)->destroy) (obj);
}

static void
schemas_file_view_finalize (GObject *obj)
{
	SchemasFileView *sfile_view;

	g_return_if_fail (IS_SCHEMAS_FILE_VIEW (obj));

	sfile_view = SCHEMAS_FILE_VIEW (obj);
	g_free (sfile_view->priv);
	sfile_view->priv = NULL;

	if (G_OBJECT_CLASS (parent_class)->finalize)
		(* G_OBJECT_CLASS (parent_class)->finalize) (obj);
}

static void
schemas_file_view_init (SchemasFileView *sfile_view)
{
	SchemasFileViewPrivate *priv;

	priv = g_new0 (SchemasFileViewPrivate, 1);
	priv->ui_manager = NULL;
	priv->sfile = NULL;
	priv->list_view = NULL;

	sfile_view->priv = priv;
}

static void
schemas_file_view_class_init (SchemasFileViewClass *klass)
{
	GObjectClass	*gobject_class;
	GtkObjectClass	*object_class;
	GtkWidgetClass	*widget_class;

	gobject_class = (GObjectClass *) klass;
	object_class = (GtkObjectClass *) klass;
	widget_class = (GtkWidgetClass *) klass;

	parent_class = g_type_class_peek_parent (klass);

	object_class->destroy = schemas_file_view_destroy;
	gobject_class->finalize = schemas_file_view_finalize;
}

static void
action_FileOpen_cb (GtkAction *action, gpointer data)
{
	GtkWidget *dialog;
	GtkWidget *window;
	SchemasFileView *sfile_view;

	sfile_view = SCHEMAS_FILE_VIEW (data);
	window = gtk_widget_get_toplevel (GTK_WIDGET (sfile_view));

	dialog = gtk_file_chooser_dialog_new ("Open File",
					      NULL,
					      GTK_FILE_CHOOSER_ACTION_OPEN,
					      GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
					      GTK_STOCK_OPEN, GTK_RESPONSE_ACCEPT,
					      NULL);

	if (gtk_dialog_run (GTK_DIALOG (dialog)) == GTK_RESPONSE_ACCEPT) {
		gchar *filename;
		SchemasFile *sfile;

		filename = gtk_file_chooser_get_filename (GTK_FILE_CHOOSER (dialog));

		sfile = schemas_file_new (filename);

		/* If .schemas file was loaded successfully. */
		if (sfile) {
			gchar *title;

			/* Load the .schemas file from the SchemasFile model.*/
			schemas_file_view_load (sfile_view, sfile);

			/* Set the window's title to reflect the filename. */
			title = g_strdup_printf ("GConf Schema Writer - %s",
						 filename);
			gtk_window_set_title (GTK_WINDOW (window), title);
			
			/* No Memory Leaks, POR FAVOR! ;-) */
			g_free (title);
		}

		g_free (filename);
	}

	gtk_widget_destroy (dialog);
}

static void
action_FileSave_cb (GtkAction *action, gpointer user_data)
{
	g_message ("%s: implement me!", __FUNCTION__);
}

static void
action_FileSaveAs_cb ()
{
	g_message ("%s: implement me!", __FUNCTION__);
}

static void
action_EditAddKey_cb ()
{
	g_message ("%s: implement me!", __FUNCTION__);
}

static void
action_EditDelKey_cb ()
{
	g_message ("%s: implement me!", __FUNCTION__);
}

static void
action_EditEditKey_cb (GtkAction *action, gpointer user_data)
{
	SchemasFileView *sfile_view = SCHEMAS_FILE_VIEW (user_data);

	GtkTreeView      *treeview = GTK_TREE_VIEW (sfile_view->priv->list_view);
	GtkTreeSelection *sel;
	GtkTreeModel     *model;
	GtkTreeIter       iter;

	sel = gtk_tree_view_get_selection (treeview);
	model = gtk_tree_view_get_model (treeview);

	if (gtk_tree_selection_get_selected (sel, &model, &iter)) {
		GtkWidget *dialog;
		Schema *s;

		/* Get the Schema object for the selected row in the view. */
		gtk_tree_model_get (model, &iter, COL_PTR, &s, -1);

		/* Load the Schema/Key in a editor dialog. */
		dialog = schema_editor_dialog_new (s);
		gtk_dialog_run (GTK_DIALOG (dialog));
		gtk_widget_destroy (dialog);

		/* Update the GtkTreeView with changes made in the editor. */
		gtk_list_store_set (GTK_LIST_STORE (model), &iter,
				    COL_PTR, s,
				    COL_OWNER, s->owner,
				    COL_KEY, s->applyto,
				    COL_TYPE, s->type,
				    -1);
 	}
}

/* Menu/Toolbar callback definitions */
static GtkActionEntry entries[] = {
	{ "Open", GTK_STOCK_OPEN, "_Open", "<control>O", "Open a schema file", action_FileOpen_cb },
	{ "Save", GTK_STOCK_SAVE, "_Save", "<control>S", "Save the schema file", action_FileSave_cb },
	{ "SaveAs", GTK_STOCK_SAVE_AS, "Save _As", "<shift><control>S", "Save the schema file under another name", action_FileSaveAs_cb },

	{ "EditMenu", NULL, "_Edit" },
	{ "AddKey", GTK_STOCK_ADD, "Add Key", "<control>plus", "Add a key to the schemas file", action_EditAddKey_cb },
	{ "DelKey", GTK_STOCK_REMOVE, "Remove Key", "Delete", "Remove a key from the schemas file", action_EditDelKey_cb },
	{ "EditKey", NULL, "Edit Key", "Return", "Edit the key in the schemas file", action_EditEditKey_cb },
};

/* Menu/Toolbar definitions. */
static const char *ui_description =
"<ui>"
"  <menubar name='MainMenu'>"
"    <menu action='FileMenu'>"
"      <placeholder name='FileMenuAdditions'>"
"        <menuitem action='Open'/>"
"        <menuitem action='Save'/>"
"        <menuitem action='SaveAs'/>"
"      </placeholder>"
"    </menu>"
"    <placeholder name='MainMenuAdditions'>"
"       <menu action='EditMenu'>"
"        <menuitem action='AddKey'/>"
"        <menuitem action='DelKey'/>"
"	 <menuitem action='EditKey'/>"
"	 <separator/>"
"      </menu>"
"    </placeholder>"
"  </menubar>"
"  <toolbar action='toolbar1'>"
"    <placeholder name='ToolbarAdditions'>"
"      <toolitem action='Open'/>"
"      <toolitem action='Save'/>"
"      <toolitem action='SaveAs'/>"
"      <separator/>"
"      <toolitem action='AddKey'/>"
"      <toolitem action='DelKey'/>"
"      <toolitem action='EditKey'/>"
"    </placeholder>"
"  </toolbar>"
"</ui>";

/*
 * Gets called for every double click on a particular row in the editor.
 */
static void
list_view_row_activated_cb (GtkTreeView *treeview, GtkTreePath *path,
			    GtkTreeViewColumn *col, gpointer user_data)
{
	GtkTreeModel *model;
	GtkTreeIter   iter;

	model = gtk_tree_view_get_model (treeview);
	if (gtk_tree_model_get_iter (model, &iter, path)) {
		GtkWidget *dialog;
		Schema *s;

		/* Get the Schema object for the selected row in the view. */
		gtk_tree_model_get (model, &iter, COL_PTR, &s, -1);

		/* Load the Schema/Key in a editor dialog. */
		dialog = schema_editor_dialog_new (s);
		gtk_dialog_run (GTK_DIALOG (dialog));
		gtk_widget_destroy (dialog);

		/* Update the GtkTreeView with changes made in the editor. */
		gtk_list_store_set (GTK_LIST_STORE (model), &iter,
				    COL_PTR, s,
				    COL_OWNER, s->owner,
				    COL_KEY, s->applyto,
				    COL_TYPE, s->type,
				    -1);
 	}
}

static GtkWidget *
schemas_file_view_construct_list_view ()
{
	GtkTreeViewColumn   *col;
	GtkCellRenderer     *renderer;
	GtkTreeModel        *model;
	GtkWidget           *view;

	view = gtk_tree_view_new ();

	renderer = gtk_cell_renderer_text_new ();
	gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (view),
						     -1,      
						     "Owner",  
						     renderer,
						     "text", COL_OWNER,
						     NULL);
	
	col = gtk_tree_view_column_new();

	renderer = gtk_cell_renderer_text_new ();
	gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (view),
						     -1,      
						     "Key",  
						     renderer,
						     "text", COL_KEY,
						     NULL);

	renderer = gtk_cell_renderer_text_new ();
	gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (view),
						     -1,      
						     "Type",  
						     renderer,
						     "text", COL_TYPE,
						     NULL);

	/* Create an empty model. */
	model = GTK_TREE_MODEL (gtk_list_store_new (NUM_COLS,
						    G_TYPE_STRING,
						    G_TYPE_STRING,
						    G_TYPE_STRING,
						    G_TYPE_POINTER,
						    -1));

	gtk_tree_view_set_model (GTK_TREE_VIEW (view), model);
	g_object_unref (model); /* destroy model automatically with view */

	/* Register for double clicks. */
	g_signal_connect (GTK_TREE_VIEW (view), "row-activated", 
			  (GCallback) list_view_row_activated_cb, NULL);

	return view;
}

static void
schemas_file_view_construct (SchemasFileView *sfile_view)
{
	GtkActionGroup *action_group;
	GtkUIManager *ui_manager;
	GtkAccelGroup *accel_group;
	GError *error;
	GtkWidget *list_view;
	GtkWidget *sw;

	/* Setup menus for merge. */
	action_group = gtk_action_group_new ("SchemasFileViewActions");
	gtk_action_group_add_actions (action_group, entries,
				      G_N_ELEMENTS (entries),
				      GTK_WIDGET (sfile_view));

	/* We already have the GtkUIManager setup. */
	ui_manager = sfile_view->priv->ui_manager;
	gtk_ui_manager_insert_action_group (ui_manager, action_group, 0);

	/* Add accelarators */
	accel_group = gtk_ui_manager_get_accel_group (ui_manager);
	gtk_window_add_accel_group (GTK_WINDOW (gtk_widget_get_toplevel (GTK_WIDGET (gtk_ui_manager_get_widget (ui_manager, "/MainMenu")))), accel_group);

	/* Actually merge the menus! */
	error = NULL;
	if (!gtk_ui_manager_add_ui_from_string (ui_manager,
						ui_description, -1, &error)) {
		g_message ("%s: building menus failed: %s", __FUNCTION__, error->message);
		g_error_free (error);
		gtk_main_quit ();
		exit (EXIT_FAILURE);
	}

	/* The ListView (in a scrolled window). */
	sw = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (sw),
					GTK_POLICY_AUTOMATIC,
					GTK_POLICY_AUTOMATIC);
	gtk_box_pack_start (GTK_BOX (sfile_view), sw, TRUE, TRUE, 0);
	gtk_widget_show (sw);

	list_view = schemas_file_view_construct_list_view ();
	gtk_container_add (GTK_CONTAINER (sw), list_view);
	gtk_widget_show (list_view);

	sfile_view->priv->list_view = list_view;
}

GType
schemas_file_view_get_type ()
{
	static GType sfile_view_type = 0;

	if (!sfile_view_type) {
		static const GTypeInfo sfile_view_type_info = {
			sizeof (SchemasFileViewClass),
			NULL,
			NULL,
			(GClassInitFunc) schemas_file_view_class_init,
			NULL,
			NULL,
			sizeof (SchemasFileView),
			0,
			(GInstanceInitFunc) schemas_file_view_init,
		};
		
		sfile_view_type = g_type_register_static (GTK_TYPE_VBOX, "SchemasFileView", &sfile_view_type_info, 0);
	}

	return sfile_view_type;
}

GtkWidget *
schemas_file_view_new (GtkUIManager *ui_manager)
{
	GtkWidget *sfile_view;

	sfile_view = GTK_WIDGET (g_object_new (TYPE_SCHEMAS_FILE_VIEW, NULL));

	/* Save reference/pointer to the GtkUIManager */
	SCHEMAS_FILE_VIEW (sfile_view)->priv->ui_manager = ui_manager;

	/* Construct rest of view. */
	schemas_file_view_construct (SCHEMAS_FILE_VIEW (sfile_view));

	return sfile_view;
}

static void
schemas_file_view_load_schema (gpointer data, gpointer user_data)
{
	GtkListStore *store = GTK_LIST_STORE (user_data);
	GtkTreeIter  iter;

	Schema *s = (Schema *) data;

	/*
	 * Interesting to note is that we store a pointer to a Schema
	 * struct and also shortcut some common keys which we show as
	 * separate. This is to get the minute performance boost in cell
	 * renderers. We don't just store pointer to our Schema objects
	 * and connect to the render signals of the tree view to render
	 * desired fields.
	 */
	gtk_list_store_append (store, &iter);
	gtk_list_store_set (store, &iter,
			    COL_PTR, s,
			    COL_OWNER, s->owner,
			    COL_KEY, s->applyto,
			    COL_TYPE, s->type,
			    -1);
}

void
schemas_file_view_load (SchemasFileView *sfile_view, SchemasFile *sfile)
{
	GtkTreeModel *model;
	GtkTreeView  *view;

	g_return_if_fail (SCHEMAS_FILE_VIEW (sfile_view));
	g_return_if_fail (sfile_view != NULL);

	view = GTK_TREE_VIEW (sfile_view->priv->list_view);

	/*
	 * Dissassociate the GtkTreeModel with the GtkTreeView.
	 * Since we're going to add stuff to the TreeModel, the updates to
	 * the GtkTreeView at each 'add' can be performance intensive.
	 */
	model = gtk_tree_view_get_model (view);
	g_object_ref (model);
	gtk_tree_view_set_model (view, NULL);

	g_list_foreach (sfile->schemas_list,
			(GFunc)schemas_file_view_load_schema, model);

	/* Reassociate the model with the tree. */
	gtk_tree_view_set_model (view, model);
	g_object_unref (model);
}
