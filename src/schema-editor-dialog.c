/*
 * schema-editor-dialog.c - A dialog for editing information in a schema for
 * a particular GConf Key.
 * 
 * Copyright (C) 2004 Archit Baweja
 *
 * Author(s):
 *	Archit Baweja <bighead@users.sourceforge.net>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <gtk/gtk.h>
#include <glade/glade.h>

#include "schema-editor-dialog.h"
#include "schema.h"

enum {
	TYPE_INT = 0,
	TYPE_STRING,
	TYPE_FLOAT,
	TYPE_BOOL,
	NUM_TYPES /* Used for for-loops. Allows to get length of this enum */
};

static const char *types[] = {
	"int",
	"string",
	"float",
	"bool"
};

	
/*
 * Following struct allows us to transfer some common data for the dialog
 * to various methods via only one reference.
 */
typedef struct _SchemaEditorDialogData SchemaEditorDialogData;

struct _SchemaEditorDialogData {
	GladeXML *gui;
	GtkWidget *type_combo;
	Schema *schema;
};


/* Called when "Type" ComboBox is changed by user. */
static void
type_combo_changed_cb (GtkComboBox *combo_box, gpointer user_data)
{
	SchemaEditorDialogData *data = (SchemaEditorDialogData *) user_data;
	GtkWidget *default_wds;
	Schema *s = data->schema;
	gint index = gtk_combo_box_get_active (combo_box);

	/*
	 * Save the changed value. SchemasFileView will handle changing the
	 * the TreeView.
	 */
	g_free (s->type);
	s->type = g_strdup (types[index]);

	/* Change the default entry widget corresponding to the type. */
	default_wds = glade_xml_get_widget (data->gui, "default_widgets");
	gtk_notebook_set_current_page (GTK_NOTEBOOK (default_wds), index);
}

/* Called when the "Key" GtkEntry changes. */
static void
key_entry_changed_cb (GtkEditable *entry, gpointer user_data)
{
	Schema *s = (Schema *) user_data;
	g_free (s->applyto);
	s->applyto = g_strdup (gtk_entry_get_text (GTK_ENTRY (entry)));
}

static void
schema_editor_dialog_load_schema (SchemaEditorDialogData *data)
{
	GladeXML *gui = data->gui;
	Schema *s = data->schema;
	GtkWidget *type_combo = data->type_combo;
	GtkWidget *tb_owner;
	GtkWidget *tb_key;
	GtkWidget *default_wds;
	gint index = 0;
	GtkWidget *default_bool;    /* For default values for bool.   */
	GtkWidget *default_string;  /* For default values for string. */
	GtkWidget *default_int;     /* For default values for int.    */
	GtkWidget *default_float;   /* For default values for float.  */

	tb_owner = glade_xml_get_widget (gui, "tb_owner");
	tb_key = glade_xml_get_widget (gui, "tb_key");
	default_wds = glade_xml_get_widget (gui, "default_widgets");

	default_int = glade_xml_get_widget (gui, "default_int");
	default_string = glade_xml_get_widget (gui, "default_string");
	default_float = glade_xml_get_widget (gui, "default_float");
	default_bool = glade_xml_get_widget (gui, "default_bool");

	gtk_entry_set_text (GTK_ENTRY (tb_owner), s->owner);
	gtk_entry_set_text (GTK_ENTRY (tb_key), s->applyto);

	/* Prepare 'type' drop down list. */
	if (!strcmp (s->type, types[TYPE_INT])) {
		index = TYPE_INT;
	} else if (!strcmp (s->type, types[TYPE_STRING])) {
		index = TYPE_STRING;
	} else if (!strcmp (s->type, types[TYPE_FLOAT])) {
		index = TYPE_FLOAT;
	} else if (!strcmp (s->type, types[TYPE_BOOL])) {
		index = TYPE_BOOL;
	} else {
		/* FIXME: what about unknown types? */
	}
	gtk_combo_box_set_active (GTK_COMBO_BOX (type_combo), index);

	/*
	 * Prepare 'default value' input widgets. GtkEntry for string,
	 * GtkComboBox for bool, GtkSpinbutton for int/floats etc.
	 */
	gtk_notebook_set_current_page (GTK_NOTEBOOK (default_wds), index);
	switch (index) {
	case TYPE_INT:
		gtk_spin_button_set_value (GTK_SPIN_BUTTON (default_int),
					   atoi (s->default_val));
		break;
	case TYPE_STRING:
		gtk_entry_set_text (GTK_ENTRY (default_string), s->default_val);
		break;
	case TYPE_BOOL:
		if ((!strcmp ("true", s->default_val)) || (!strcmp ("TRUE", s->default_val))) {
			gtk_combo_box_set_active (GTK_COMBO_BOX (default_bool),
						  0);
		} else {
			gtk_combo_box_set_active (GTK_COMBO_BOX (default_bool),
						  1);
		}

		break;
	case TYPE_FLOAT:
		gtk_spin_button_set_value (GTK_SPIN_BUTTON (default_float), 
					   atof (s->default_val));
		break;
	default:
		/* FIXME: what about unknown types? */
		;
	}

	/* Connect signals to get changes to the schema info. */
	g_signal_connect (G_OBJECT (type_combo), "changed",
			  G_CALLBACK (type_combo_changed_cb), (gpointer) s);

	g_signal_connect (G_OBJECT (tb_key), "changed",
			  G_CALLBACK (key_entry_changed_cb), (gpointer) s);
}

GtkWidget *
schema_editor_dialog_new (Schema *s)
{
	SchemaEditorDialogData *data;
	GladeXML *gui;
	GtkWidget *type_combo;
	GtkWidget *combo_placeholder;
	GtkWidget *default_wds;
	int i;

	gui = glade_xml_new (GLADEDIR "schema-editor-dialog.glade",
			     NULL, NULL);

	combo_placeholder = glade_xml_get_widget (gui, "cb_type_placeholder");
	default_wds = glade_xml_get_widget (gui, "default_widgets");

	/* Create the GtkComboBox for "type" of schema. */
	type_combo = gtk_combo_box_new_text ();
	for (i = 0; i < NUM_TYPES; i++)
		gtk_combo_box_append_text (GTK_COMBO_BOX (type_combo),
					   types[i]);
	gtk_container_add (GTK_CONTAINER (combo_placeholder), type_combo);
	gtk_widget_show (type_combo);

	gtk_notebook_set_show_tabs (GTK_NOTEBOOK (default_wds), FALSE);

	data = g_new (SchemaEditorDialogData, 1);
	data->gui = gui;
	data->type_combo = type_combo;
	data->schema = s;

	schema_editor_dialog_load_schema (data);

	return glade_xml_get_widget (gui, "schema-editor-dialog");
}
