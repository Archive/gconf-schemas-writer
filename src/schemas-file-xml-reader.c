/*
 * schemas-file-xml-reader.c - A parser for .schemas xml files for GConf.
 * 
 * Copyright (C) 2004 Archit Baweja
 *
 * Author(s):
 *	Archit Baweja <bighead@users.sourceforge.net>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <glib.h>
#include <libxml/xmlmemory.h>
#include <libxml/parser.h>

#include "schemas-file-xml-reader.h"
#include "schema.h"

#ifdef DEBUG
#define debug(...) fprintf(stderr, __VA_ARGS__)
#else
#define debug(...) 
#endif

static void
debug_print_schema (Schema *schema)
{
	debug ("Schema Info: \n");
	debug ("\tkey: %s\n", schema->key);
	debug ("\tapplyto: %s\n", schema->applyto);
	debug ("\ttype: %s\n", schema->type);
}

static void
debug_print_list (gpointer data, gpointer user_data)
{
	debug_print_schema ((Schema *) data);
}

static gint
current_node_is (xmlNodePtr cur, gchar *str)
{
	return !xmlStrcmp (cur->name, (const xmlChar *) str);
}

static void
parse_schema (Schema *s, xmlNodePtr cur)
{
	while (cur != NULL) {
		if (cur->type == XML_ELEMENT_NODE) {

			if (current_node_is (cur, "locale")) {
				/* FIXME: Ignoring locale data for now. */
			} else if (current_node_is (cur, "key")) {
				s->key = g_strdup (cur->xmlChildrenNode->content);
			} else if (current_node_is (cur, "applyto")) {
				s->applyto = g_strdup (cur->xmlChildrenNode->content);
			} else if (current_node_is (cur, "type")) {
				s->type = g_strdup (cur->xmlChildrenNode->content);
			} else if (current_node_is (cur, "owner")) {
				s->owner = g_strdup (cur->xmlChildrenNode->content);
			} else if (current_node_is (cur, "default")) {
				s->default_val = g_strdup (cur->xmlChildrenNode->content);
			} else {
				/* FIXME: We ignore any other data. */
			}
		}

		cur = cur->next;
	}

	return;
}

static gboolean
parse_schemalist (SchemasFileXMLReader *this, xmlNodePtr cur)
{
	GList *schemas_list = NULL;
	Schema *s = NULL;

	while (cur != NULL) {
		if (cur->type == XML_ELEMENT_NODE) {
			if (xmlStrcmp (cur->name, (const xmlChar *) "schema")) {
				debug("Malformed GConf Schema XML!\n");
				debug("Instead found Node: %s.\n", cur->name);
				/* Error in parsing. :-/ */
				return FALSE;
			} else {
				/* New Schema object for each entry. */
				s = schema_new ();

				parse_schema (s, cur->xmlChildrenNode);

				/* Add schema to list. */
				schemas_list = g_list_append (schemas_list, s);
			}
		}

		cur = cur->next;
	}

	/* Save schemas_list pointer. */
	this->schemas_list = schemas_list;

	return TRUE;
}

static SchemasFileXMLReader *
schemas_file_xml_reader_init ()
{
	SchemasFileXMLReader *this;

	this = g_new0 (SchemasFileXMLReader, 1);
	this->doc = NULL;
	this->filename = NULL;
	this->schemas_list = NULL;

	return this;
}

SchemasFileXMLReader *
schemas_file_xml_reader_new(const gchar *filename)
{
	SchemasFileXMLReader *this;
	xmlDocPtr doc;
	xmlNodePtr cur;
	gboolean ret = TRUE;

	this = schemas_file_xml_reader_init (); 

	/* Parse and validate document. */
	doc = xmlParseFile (filename);
	this->doc = doc;

	if (doc == NULL) {
		debug("Document not parsed successfully!\n");
		return NULL;
	}

	cur = xmlDocGetRootElement (doc);

	if (cur == NULL ||
	    xmlStrcmp (cur->name, (const xmlChar *) "gconfschemafile")) {
		debug("NO! Not a valid GConf schema file. ");
		debug("Instead found Node: %s.\n", cur->name);

		schemas_file_xml_reader_free (this);
		return NULL;
	} else {
		debug("Found <gconfschemafile> tag. Everything good.\n");
	}

	/* Cycle through all <schemalist> children. */
	cur = cur->xmlChildrenNode;

	while (cur != NULL) {
		if (cur->type == XML_ELEMENT_NODE) {
			if (xmlStrcmp (cur->name, (const xmlChar *) "schemalist")) {
				debug("Bad content in <schemalist>. ");
				debug("Instead found Node: %s.\n", cur->name);
				xmlFreeDoc (doc);
				return NULL;
			} else {
				debug("Found <schemalist> tag. Everything good.\n");

				debug("Loading schemas...\n");

				/* Load all schemas in a list from the xml. */
				ret = parse_schemalist (this, cur->xmlChildrenNode);

				/* Error in parsing! */
				if (ret == FALSE) {
					debug("OMG ERROR IN PARSING <schemalist> children!\n");
					return NULL;
				}

			}
		}

		cur = cur->next;
	}

	debug("schemas_list.length = %d\n",g_list_length (this->schemas_list));

	return this;
}

void
schemas_file_xml_reader_free (SchemasFileXMLReader *this)
{
	if (this->doc != NULL) {
		xmlFreeDoc (this->doc);
		this->doc = NULL;
	}

	if (this->filename != NULL) {
		g_free (this->filename);
		this->filename = NULL;
	}

	if (this->schemas_list != NULL) {
		g_list_free (this->schemas_list);
		this->schemas_list = NULL;
	}
}

const GList *
schemas_file_xml_reader_get_schemas_list (SchemasFileXMLReader *this)
{
	return this->schemas_list;
}
